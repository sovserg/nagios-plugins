#!/usr/bin/perl -w
#
# check_akcp_sensorprobe.pl v1.0
#
# version history
#
# 1.0 first release  
#
# Nagios plugin script for checking AKCP SensorProbe Devices.
#
# License: GPL v2
# Copyright (c) 2018 Davide "Argaar" Foschi
#

use strict 'vars';
no warnings 'uninitialized';
use Net::SNMP qw(ticks_to_time);;
use Switch;
use Getopt::Std;

# Command arguments
my %options=();
getopts("H:C:l:P:i:s:w:c:p", \%options);

# Help message etc
(my $script_name = $0) =~ s/.\///;

my $help_info = <<END;
\n$script_name - v1.0

Nagios script to check values of sensorProbe devices.

Usage:
-H  Address or hostname of Patton (required)
-C  SNMP community string (required)
-l  Command (optional, see command list)
-P  SNMP port (optional, defaults to port 161)
-t  Connection timeout (optional, default 10s)
-s  Number of sensors managed by the probe (optional, default 8)
-i  Sensor Index Number (optional)
-w  Warning threshold (optional, see commands)
-c  Critical threshold (optional, see commands)
-p  Output performance data (optional, default no)

Commands (supplied with -l argument):

    temperature
    humidity
    switch
        Retrieve the status of current switch sensors and check if against an expected result if not match it return a critical exit
        This command DOES NOT accepts "W" and "C" thresholds as it can't monitor values, rather only the current status and the expected ones, however it returns
        OKAY, WARNING and CRITICAL based on the switch status.
        

Example:
$script_name -H mysensorprobe.domain.local -C public -l temperature

END

# OIDs for the checks
my $oid_temperatureSensor_base                   = "1.3.6.1.4.1.3854.1.2.2.1.16.1";
my $oid_humiditySensor_base                      = "1.3.6.1.4.1.3854.1.2.2.1.17.1";
my $oid_switchSensor_base                        = "1.3.6.1.4.1.3854.1.2.2.1.18.1";

# Nagios exit codes
my $OKAY        = 0;
my $WARNING     = 1;
my $CRITICAL    = 2;
my $UNKNOWN     = 3;

# Command arguments and defaults
my $snmp_host           = $options{H};
my $snmp_community      = $options{C};
my $snmp_port           = $options{P} || 161;   # SNMP port default is 161
my $connection_timeout  = $options{t} || 10;    # Connection timeout default 10s
my $sensor_num          = $options{s} || 8;     # Default 8 sensors probe
my $sensor_idx          = $options{i};
my $default_error       = $UNKNOWN;
my $check_command       = $options{l};
my $critical_threshold  = (!defined $options{c}) ? 999 : $options{c};
my $warning_threshold   = (!defined $options{w}) ? 999 : $options{w};
my $output_perfdata     = (!defined $options{p}) ? 0 : 1;
my $session;
my $error;
my $exitCode;
my $exitString;

#Maximum length of 15 characters for snmp community strings
if(defined $snmp_community) {$snmp_community = substr($snmp_community,0,15);}

# If we don't have the needed command line arguments exit with UNKNOWN.
if(!defined $options{H} || !defined $options{C}){
    print "$help_info\n\n--> ERROR <--\n--> Not all required options were specified. <--\n\n";
    exit $UNKNOWN;
}

# Setup the SNMP session
($session, $error) = Net::SNMP->session(
    -hostname   => $snmp_host,
    -community  => $snmp_community,
    -timeout    => $connection_timeout,
    -port       => $snmp_port,
    -translate  => [-timeticks => 0x0]
);

# If we cannot build the SNMP session, error and exit
if (!defined $session) {
    printf "$$default_error: %s\n", $error;
    exit $default_error;
}

# Determine what we need to do based on the command input
if (!defined $options{l}) {  # If no command was given, inform user
    print "No command was specified, please provide valid one to check the probe for.\n";
    exit $OKAY;
} else {    # Process the supplied command. Script will exit as soon as it has a result.
    switch($check_command){
        case "temperature" {
            $exitCode = $OKAY;
            $exitString = "OK";
            my @tempData = ();
            my @tempPerfData = ();
            if (!defined $sensor_idx) {
                my $sensors_available = 0;
                for (my $i=0; $i<=$sensor_num-1; $i++) {
                    my $tempsensor_name = query_oid($oid_temperatureSensor_base . ".1." . $i);
                    my $tempsensor_status = query_oid($oid_temperatureSensor_base . ".4." . $i);
                    my $tempsensor_value = query_oid($oid_temperatureSensor_base . ".3." . $i);

                    my @crit_values = ();
                    if ($critical_threshold == 999) {
                        my $tempsensor_HC_value = query_oid($oid_temperatureSensor_base . ".8." . $i);
                        my $tempsensor_LC_value = query_oid($oid_temperatureSensor_base . ".10." . $i);
                        push @crit_values, $tempsensor_LC_value;
                        push @crit_values, $tempsensor_HC_value;
                    } else {
                        @crit_values = split(/:/, $critical_threshold);
                    }

                    my @warn_values = ();
                    if ($warning_threshold == 999) {
                        my $tempsensor_HW_value = query_oid($oid_temperatureSensor_base . ".7." . $i);
                        my $tempsensor_LW_value = query_oid($oid_temperatureSensor_base . ".9." . $i);
                        push @warn_values, $tempsensor_LW_value;
                        push @warn_values, $tempsensor_HW_value;
                    } else {
                        @warn_values = split(/:/, $warning_threshold);
                    }

                    if ($tempsensor_status != 0) {
                        $sensors_available++;
                        if ($crit_values[0]>$tempsensor_value || $crit_values[1]<$tempsensor_value) {
                            $exitCode = $CRITICAL;
                            $exitString = "CRITICAL";
                        }
                        if (($warn_values[0]>$tempsensor_value || $warn_values[1]<$tempsensor_value) && $exitCode != $CRITICAL) {
                            $exitCode = $WARNING;
                            $exitString = "WARNING";
                        }
                        push @tempData, "$tempsensor_name: $tempsensor_value°C";
                        push @tempPerfData, "'$tempsensor_name'=$tempsensor_value;$warn_values[0]:$warn_values[1];$crit_values[0]:$crit_values[1]";
                    }
                }
                if ($sensors_available == 0) {
                    $exitCode = $UNKNOWN;
                    $exitString = "UNKNOWN - No sensors available";
                }
                if ($output_perfdata == 0) {
                    print $exitString . " - " . join(", ", @tempData) . "\n";
                } else {
                    print $exitString . " - " . join(", ", @tempData) . "|" . join(" ", @tempPerfData) . "\n";
                }
            } else {
                my $tempsensor_name = query_oid($oid_temperatureSensor_base . ".1." . $sensor_idx);
                my $tempsensor_status = query_oid($oid_temperatureSensor_base . ".4." . $sensor_idx);
                my $tempsensor_value = query_oid($oid_temperatureSensor_base . ".3." . $sensor_idx);

                my @crit_values = ();
                if ($critical_threshold == 999) {
                    my $tempsensor_HC_value = query_oid($oid_temperatureSensor_base . ".8." . $sensor_idx);
                    my $tempsensor_LC_value = query_oid($oid_temperatureSensor_base . ".10." . $sensor_idx);
                    push @crit_values, $tempsensor_LC_value;
                    push @crit_values, $tempsensor_HC_value;
                } else {
                    @crit_values = split(/:/, $critical_threshold);
                }

                my @warn_values = ();
                if ($warning_threshold == 999) {
                    my $tempsensor_HW_value = query_oid($oid_temperatureSensor_base . ".7." . $sensor_idx);
                    my $tempsensor_LW_value = query_oid($oid_temperatureSensor_base . ".9." . $sensor_idx);
                    push @warn_values, $tempsensor_LW_value;
                    push @warn_values, $tempsensor_HW_value;
                } else {
                    @warn_values = split(/:/, $warning_threshold);
                }

                if ($tempsensor_status != 0) {
                    if ($crit_values[0]>$tempsensor_value || $crit_values[1]<$tempsensor_value) {
                        $exitCode = $CRITICAL;
                        $exitString = "CRITICAL";
                    }
                    if (($warn_values[0]>$tempsensor_value || $warn_values[1]<$tempsensor_value) && $exitCode != $CRITICAL) {
                        $exitCode = $WARNING;
                        $exitString = "WARNING";
                    }
                    push @tempData, "$tempsensor_name: $tempsensor_value°C";
                    push @tempPerfData, "'$tempsensor_name'=$tempsensor_value;$warn_values[0]:$warn_values[1];$crit_values[0]:$crit_values[1]";
                } else {
                    $exitCode = $UNKNOWN;
                    $exitString = "UNKNOWN - No data received";
                }
                if ($output_perfdata == 0) {
                    print $exitString . " - " . join(", ", @tempData) . "\n";
                } else {
                    print $exitString . " - " . join(", ", @tempData) . "|" . join(" ", @tempPerfData) . "\n";
                }
            }
            $session->close();
            exit $exitCode;
        }
        case "humidity" {

            my @tempData = ();
            my @tempPerfData = ();
            if (!defined $sensor_idx) {
                my $sensors_available = 0;
                for (my $i=0; $i<=$sensor_num-1; $i++) {
                    my $humsensor_name = query_oid($oid_humiditySensor_base . ".1." . $i);
                    my $humsensor_status = query_oid($oid_humiditySensor_base . ".4." . $i);
                    my $humsensor_value = query_oid($oid_humiditySensor_base . ".3." . $i);

                    my @crit_values = ();
                    if ($critical_threshold == 999) {
                        my $humsensor_HC_value = query_oid($oid_humiditySensor_base . ".8." . $i);
                        my $humsensor_LC_value = query_oid($oid_humiditySensor_base . ".10." . $i);
                        push @crit_values, $humsensor_LC_value;
                        push @crit_values, $humsensor_HC_value;
                    } else {
                        @crit_values = split(/:/, $critical_threshold);
                    }

                    my @warn_values = ();
                    if ($warning_threshold == 999) {
                        my $humsensor_HW_value = query_oid($oid_humiditySensor_base . ".7." . $i);
                        my $humsensor_LW_value = query_oid($oid_humiditySensor_base . ".9." . $i);
                        push @warn_values, $humsensor_LW_value;
                        push @warn_values, $humsensor_HW_value;
                    } else {
                        @warn_values = split(/:/, $warning_threshold);
                    }

                    if ($humsensor_status != 0) {
                        $sensors_available++;
                        if ($crit_values[0]>$humsensor_value || $crit_values[1]<$humsensor_value) {
                            $exitCode = $CRITICAL;
                            $exitString = "CRITICAL";
                        }
                        if (($warn_values[0]>$humsensor_value || $warn_values[1]<$humsensor_value) && $exitCode != $CRITICAL) {
                            $exitCode = $WARNING;
                            $exitString = "WARNING";
                        }
                        push @tempData, "$humsensor_name: $humsensor_value";
                        push @tempPerfData, "'$humsensor_name'=$humsensor_value;$warn_values[0]:$warn_values[1];$crit_values[0]:$crit_values[1]";
                    }
                }
                if ($sensors_available == 0) {
                    $exitCode = $UNKNOWN;
                    $exitString = "UNKNOWN - No sensors available";
                }
                if ($output_perfdata == 0) {
                    print $exitString . " - " . join(", ", @tempData) . "\n";
                } else {
                    print $exitString . " - " . join(", ", @tempData) . "|" . join(" ", @tempPerfData) . "\n";
                }
            } else {
                my $humsensor_name = query_oid($oid_humiditySensor_base . ".1." . $sensor_idx);
                my $humsensor_status = query_oid($oid_humiditySensor_base . ".4." . $sensor_idx);
                my $humsensor_value = query_oid($oid_humiditySensor_base . ".3." . $sensor_idx);

                my @crit_values = ();
                if ($critical_threshold == 999) {
                    my $humsensor_HC_value = query_oid($oid_humiditySensor_base . ".8." . $sensor_idx);
                    my $humsensor_LC_value = query_oid($oid_humiditySensor_base . ".10." . $sensor_idx);
                    push @crit_values, $humsensor_LC_value;
                    push @crit_values, $humsensor_HC_value;
                } else {
                    @crit_values = split(/:/, $critical_threshold);
                }

                my @warn_values = ();
                if ($warning_threshold == 999) {
                    my $humsensor_HW_value = query_oid($oid_humiditySensor_base . ".7." . $sensor_idx);
                    my $humsensor_LW_value = query_oid($oid_humiditySensor_base . ".9." . $sensor_idx);
                    push @warn_values, $humsensor_LW_value;
                    push @warn_values, $humsensor_HW_value;
                } else {
                    @warn_values = split(/:/, $warning_threshold);
                }

                if ($humsensor_status != 0) {
                    if ($crit_values[0]>$humsensor_value || $crit_values[1]<$humsensor_value) {
                        $exitCode = $CRITICAL;
                        $exitString = "CRITICAL";
                    }
                    if (($warn_values[0]>$humsensor_value || $warn_values[1]<$humsensor_value) && $exitCode != $CRITICAL) {
                        $exitCode = $WARNING;
                        $exitString = "WARNING";
                    }
                    push @tempData, "$humsensor_name: $humsensor_value";
                    push @tempPerfData, "'$humsensor_name'=$humsensor_value;$warn_values[0]:$warn_values[1];$crit_values[0]:$crit_values[1]";
                } else {
                    $exitCode = $UNKNOWN;
                    $exitString = "UNKNOWN - No data received";
                }
                if ($output_perfdata == 0) {
                    print $exitString . " - " . join(", ", @tempData) . "\n";
                } else {
                    print $exitString . " - " . join(", ", @tempData) . "|" . join(" ", @tempPerfData) . "\n";
                }
            }

            $session->close();
            exit $exitCode;
        }
        case "switch" {
            $exitCode = $OKAY;
            $exitString = "OK";
            my @tempData = ();
            my @tempPerfData = ();
            if (!defined $sensor_idx) {
                my $sensors_available = 0;
                for (my $i=0; $i<=$sensor_num-1; $i++) {
                    my $switchsensor_name = query_oid($oid_switchSensor_base . ".1." . $i);
                    my $switchsensor_status = query_oid($oid_switchSensor_base . ".3." . $i);
                    my $switchsensor_value = query_oid($oid_switchSensor_base . ".8." . $i);
                    my $switchsensor_expected = query_oid($oid_switchSensor_base . ".7." . $i);
                    if ($switchsensor_status == 2) { #Enabled, Operative
                        $sensors_available++;
                        my $sStatus;
                        my $sExpected;
                        if ($switchsensor_value == 0) {
                            $sStatus = 'PRESENT';
                        } else {
                            $sStatus = 'ABSENT';
                        }
                        if ($switchsensor_expected == 0) {
                            $sExpected = 'PRESENT';
                        } else {
                            $sExpected = 'ABSENT';
                        }
                        if ($switchsensor_value != $switchsensor_expected) {
                            $exitCode = $CRITICAL;
                            $exitString = "CRITICAL";
                        }
                        push @tempData, "'$switchsensor_name': $sStatus";
                        push @tempPerfData, "'$switchsensor_name'=$switchsensor_value";
                    } elsif ($switchsensor_status == 7) { #Enabled, not plugged in
                        $sensors_available++;
                        push @tempData, "'$switchsensor_name' is enabled but can't be checked because is not operative, maybe unplugged?";
                        if ($exitCode != $CRITICAL) {
                            $exitCode = $WARNING;
                            $exitString = "WARNING";
                        }
                    }
                }
                if ($sensors_available == 0) {
                    $exitCode = $UNKNOWN;
                    $exitString = "UNKNOWN - No sensors available";
                }
                if ($output_perfdata == 0) {
                    print $exitString . " - " . join(', ',@tempData);
                } else {
                    if (@tempPerfData == 0) {
                        print $exitString . " - " . join(', ',@tempData);
                    } else {
                        print $exitString . " - " . join(', ',@tempData) . "|" . join(', ',@tempPerfData);
                    }
                }
                $session->close();
                exit $exitCode;
            } else {
                my $switchsensor_name = query_oid($oid_switchSensor_base . ".1." . $sensor_idx);
                my $switchsensor_status = query_oid($oid_switchSensor_base . ".3." . $sensor_idx);
                my $switchsensor_value = query_oid($oid_switchSensor_base . ".8." . $sensor_idx);
                my $switchsensor_expected = query_oid($oid_switchSensor_base . ".7." . $sensor_idx);
                if ($switchsensor_status == 2) { #Enabled, Operative
                    my $sStatus;
                    my $sExpected;
                    if ($switchsensor_value == 0) {
                        $sStatus = 'PRESENT';
                    } else {
                        $sStatus = 'ABSENT';
                    }
                    if ($switchsensor_expected == 0) {
                        $sExpected = 'PRESENT';
                    } else {
                        $sExpected = 'ABSENT';
                    }
                    if ($switchsensor_value != $switchsensor_expected) {
                        $exitCode = $CRITICAL;
                        $exitString = "CRITICAL";
                    }
                    if ($output_perfdata == 0) {
                        $exitString .= " - '$switchsensor_name': $sStatus\n";
                    } else {
                        $exitString .= " - '$switchsensor_name': $sStatus|'$switchsensor_name'=$switchsensor_value\n";
                    }
                } elsif ($switchsensor_status == 7) { #Enabled, not plugged in
                    $exitCode = $WARNING;
                    $exitString = "WARNING - '$switchsensor_name' is enabled but can't be checked because is not operative, maybe unplugged?\n";
                } else {
                    $exitCode = $UNKNOWN;
                    $exitString = "UNKNOWN - No data received";
                }
                print $exitString;
                $session->close();
                exit $exitCode;
            }
        }
        else {
            print "$script_name - '$check_command' is not a valid comand\n";
            exit $UNKNOWN;
        }

    }
}

sub query_oid {
# This function will poll the active SNMP session and return the value
# of the OID specified. Only inputs are OID. Will use global $session 
# variable for the session.
    my $oid = $_[0];
    my $response = $session->get_request(-varbindlist => [ $oid ],);

    # If there was a problem querying the OID error out and exit
    if (!defined $response) {
        my $output_header = "UNKNOWN";
        printf "$output_header: %s\n", $session->error();
        $session->close();
        exit $default_error;
    }

    return $response->{$oid};
}

# The end. We shouldn't get here, but in case we do exit unknown
print "UNKNOWN: Unknown script error\n";
exit $UNKNOWN;